# dc-iot
Exemplos de código para a plataforma Arduino, Galileo e outras usadas na IoT.

Este projeto visa o desenvolvimento de material didático para a programação em dispositivos voltados à IoT.

Acesse [http://iot.dc.ufscar.br/](http://iot.dc.ufscar.br/) para saber mais.

Acesse [aqui](https://bytebucket.org/dc-ufscar/iot/raw/master/doc/apostila/iot.pdf) a versão atual do material.