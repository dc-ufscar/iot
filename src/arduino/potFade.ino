/*
  Código de exemplo para o desenvolvimento do 2º Projeto da Apostila de
  Introdução à Internet das Coisas (IoT) usando Arduino e Intel Galileo
  
  Mais informações e material completo em: 
  http://iot.dc.ufscar.br/
    
  Departmanto de Computação
  UFSCar - São Carlos

  31/07/2015: Primeira versão - Antonio Carlos (falcaopetri@gmail.com)
*/

const int pin_pot = A0;
const int pin_led = 5;

void setup() {
  pinMode(pin_led, OUTPUT);
}

void loop() {
  int input = analogRead(pin_pot);

  /*
   * Nos Arduinos, a leitura de um pino analógico possui uma precisão de 10 bits,
   * isto é, conseguimos discretizar o intervalo 0-5V para o intervalo (de inteiros) 0-1023.
   * Por outro lado, a resolução dos pinos de saída é de 8 bits (inteiros de 0 a255).
   * Assim, precisamos achar a relação:
   *    input / output = 1023 / 255
   * => output = input * 255 / 1023
   * 
   * 
   * A função map funciona como uma Regra de Três. Estamos mapeando a variável leitura,
   * que pode assumir valores entre 0 e 1023, para um valor no intervalo de 0 a 255.
   */
  int output = map(input, 0, 1023, 0, 255);

  analogWrite(pin_led, output);
}
