// Define o pino no qual o LED esta conectado
// Alguns pinos que suportam PWM que podem ser usados:
// 3, 5, 6, 9, 10, 11

const int pinLed    = 5;

void setup()
{
    // Configurar o pino do LED como sinal de saida
    pinMode(pinLed, OUTPUT);
}

void loop()
{
    digitalWrite(pinLed, HIGH);         // faz o LED ligar
    delay(1000);                        // espera 1 segundo
    digitalWrite(pinLed, LOW);          // faz o LED desligar
    delay(1000);
}