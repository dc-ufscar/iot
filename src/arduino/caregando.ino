/*
  Código de exemplo para o desenvolvimento do 2º Projeto da Apostila de
  Introdução à Internet das Coisas (IoT) usando Arduino e Intel Galileo
  
  Mais informações e material completo em: 
  http://iot.dc.ufscar.br/
    
  Departmanto de Computação
  UFSCar - São Carlos

  03/06/2015: Primeira versão - Gabriel Silva Trevisan
  06/06/2015: Modificado por Antonio Carlos (falcaopetri@gmail.com)
  31/07/2015: Modificado por Antonio Carlos - Organização e comentários
*/

#include <Wire.h>
#include "rgb_lcd.h"

/*
 * 
 */
const char empty[] = "                ";

/*
 * Declaração do pino onde um potenciômetro deve estar conectado - Pino Analógico 0
 */
const int pin_pot = A0;

/*
 * Certifique-se que o LCD está em conectado em uma das portas I2C da Base Shield.
 * A biblioteca será capaz de reconhecê-lo, sem que precissemos definir um pino no código.
 */
rgb_lcd lcd;

/*
 * Uma estrutura auxiliar para guardar as informações de uma Cor formada pelos canais
 * Red, Green, Blue (RGB).
 * O LCD disponível no Kit Grove permite que definamos sua cor de fundo.
 */
struct Cor {
	uint8_t R;
	uint8_t G;
	uint8_t B;
};
Cor cor = {0, 0, 0};

/*
 * 
 */
// 1 coluna
byte coluna1[8] = {
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
  0b10000,
};

// 2 colunas
byte coluna2[8] = {
  0b11000,
  0b11000,
  0b11000,
  0b11000,
  0b11000,
  0b11000,
  0b11000,
  0b11000,
};

// 3 colunas
byte coluna3[8] = {
  0b11100,
  0b11100,
  0b11100,
  0b11100,
  0b11100,
  0b11100,
  0b11100,
  0b11100,
};

// 4 colunas
byte coluna4[8] = {
  0b11110,
  0b11110,
  0b11110,
  0b11110,
  0b11110,
  0b11110,
  0b11110,
  0b11110,
};

// 5 colunas
byte coluna5[8] = {
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
};

void setup() {
    // Iniciamos um LCD informando seu número de colunas (16) e linhas (2)
    lcd.begin(16, 2);

    /*
     * Definimos pares "chave-valor" que ensinam o LCD a realizar novos desenhos.
     * A partir daqui, quando escrevermos o byte 1, observaremos o desenho definido por coluna1
     */
    lcd.createChar(1, coluna1);
    lcd.createChar(2, coluna2);
    lcd.createChar(3, coluna3);
    lcd.createChar(4, coluna4);
    lcd.createChar(5, coluna5);
}


void loop() {
  int leitura = analogRead(pin_pot);  // Leitura do valor atual do Potenciômetro - um inteiro entre 0 e 1023 (incluso)
  /*
   * Com essa operação, eliminamos o primeiro dígito de nossa leitura (obtendo valores
   * múltiplos de 10 (0, 10, 20, ..., 1010, 1020).
   * Assim, eliminamos possíveis instabilidades na leitura.
   * Tente comentá-la e observar os valores no LCD
   */
  leitura = (leitura / 10) * 10;

  /*
   * Ajuste os coeficientes que multiplicam o valor mapeado
   * para qualquer valor float entre 0-1. Assim, podemos formar 
   * qualquer cor RGB.
   */
  cor.R = map(leitura, 0, 1020, 0, 255) * 1.0;
  cor.G = map(leitura, 0, 1020, 0, 255) * 1.0;
  cor.B = map(leitura, 0, 1020, 0, 255) * 1.0;

  /*
   * Definimos a cor de fundo desejada para a luz de fundo.
   */
  lcd.setRGB(cor.R, cor.G, cor.B);
  
  /*
   * A função map funciona como uma Regra de Três. Estamos mapeando a variável leitura,
   * que pode assumir valores entre 0 e 1020, para um valor no intervalo de 0 a 10.000.
   * Depois, dividimos esse valor por 100.0. Dessa forma, conseguimos o valor da leitura
   * em um percentual (de 0 a 100) com duas casas decimais de precisão.
   */
  float porcentagem = map(leitura, 0, 1020, 0, 10000) / 100.0;

  /*
   * Montamos uma mensagem como: " 58.90%         "
   */
  char msg[17];
  snprintf(msg, 17, "%6.2f%%%s", porcentagem, empty);

  /*
   * Indicamos que queremos escrever na coluna 0, linha 0.
   */
  lcd.setCursor(0,0);
  lcd.print("LED: ");
  lcd.print(msg);

  /*
   * Calculamos a quantidade de blocos que devem ser preenchidos
   * (lembrando que a linha possui 16 colunas).
   */
  int numBlocos = (porcentagem/100) * 16;
  /*
   * Calculamos a diferença entre o valor float e o valor inteiro do número
   * de blocos preenchidos. Assim, temos um valor entre 0 e 1 que indica
   * quantas fileiras do bloco incompleto devem ser preenchidas.
   * Como cada coluna possui 5 fileiras, multiplicamos esse valor por 5.
   */
  int restante = ((porcentagem/100.0 * 16.0) - numBlocos)  * 5.0;

  /*
   * Indicamos que queremos escrever na coluna 0, linha 1.
   */
  lcd.setCursor(0,1);
  
  for (int i = 0; i < numBlocos; i++) {
    lcd.write(5); // Preenchemos os blocos completos
  }
  
  if (restante != 0) {    // O caracter 0 não foi definido, então ignoraremos ele.
    lcd.write(restante);  // Preenchemos as fileiras do bloco incompleto.
  }

  for (int i = 0; i < 16 - numBlocos; i++) {
    lcd.write(" ");       // Completamos a linha com espaços, para limpar caracteres lixos
  }
}