int cont = 0;

const int n_nomes = 4;
const String nomes[] = { "Ana", "Bruna", "Carlos", "Daniel" };

void setup() {
  Serial.begin(9600);
  
  Serial.print("Hello, world.");
}

void loop() {
  Serial.print("Hello, ");
  Serial.print(nomes[cont]);
  Serial.println("!");

  cont++;
  if (cont == n_nomes) {
    cont = 0;
  }
  
  delay(500);
}