// Define o pino no qual o LED esta conectado
// Alguns pinos que suportam PWM que podem ser usados:
// 3, 5, 6, 9, 10, 11

const int pinLed = 5;

// Vamos definir um delay para o efeito esperado
// Usando um valor pequeno para um efeito mais rapido
const int BREATH_DELAY = 5; // milisegundos

void setup()
{
    // Configurar o pino do LED como sinal de saida
    pinMode(pinLed, OUTPUT);
}

void loop()
{

    for(int i=0; i<256; i++) 
    {
        analogWrite(pinLed, i);
        delay(BREATH_DELAY);
    }
    delay(100);
    
    for(int i=254; i>=0; i--)
    {
        analogWrite(pinLed, i);
        delay(BREATH_DELAY);
    }
    delay(500);
}