/*
  Código de exemplo para o desenvolvimento do 1º Projeto da Apostila de
  Introdução à Internet das Coisas (IoT) usando Arduino e Intel Galileo
  
  Mais informações e material completo em: 
  http://iot.dc.ufscar.br/
    
  Departamento de Computação
  UFSCar - São Carlos

  01/05/2015: Primeira versão - Antonio Carlos (falcaopetri@gmail.com)
  03/05/2015: Removido a codificação de números e caracteres especiais - Antonio Carlos
  20/07/2015: Segunda versão - Gabriela Mattos (gabrielavmattos@gmail.com) - Uso do botao/sensor de toque como entrada
  
*/

#define MORSE_OUT_LED   3
#define MORSE_IN        7

#define DOT  120
#define DASH 3*DOT

int valor = 1; // Para ler o estado do botao

void setup() 
{
  Serial.begin(9600); // Comunicação Serial

  pinMode(MORSE_OUT_LED, OUTPUT);
  pinMode(MORSE_IN, INPUT);  
}

void loop() 
{
  valor = digitalRead(MORSE_IN);
  if (valor == HIGH) // O codigo morse so sera apresentado quando o usuario apertar no sensor de toque
    if (Serial.available()) 
    {
      char leitura = Serial.read();    // Pega um caracter da serial por vez
      imprimirMorse(leitura);
    }
}

void imprimirMorse (char c) 
{
  c = toupper(c); // Deixa o caracter maiusculo
  String morse = getMorseCode(c);

  Serial.print(c);
  for (int i = 0; i < morse.length(); ++i) 
  {
    Serial.print(morse[i]);
    if (morse[i] == '.')
      dot();
    else if (morse[i] == '-')
      dash();
    else if (morse[i] == ' ')
      silencio();
  }
  Serial.println();
}

String getMorseCode (char c) 
{
  switch (c) 
  {
    case 'A': return ".-";
    case 'B': return "-...";
    case 'C': return "-.-.";
    case 'D': return "-..";
    case 'E': return ".";
    case 'F': return "..-.";
    case 'G': return "--.";
    case 'H': return "....";
    case 'I': return "..";
    case 'J': return "---";
    case 'K': return "-.-";
    case 'L': return ".-..";
    case 'M': return "--";
    case 'N': return "-.";
    case 'O': return "---";
    case 'P': return ".--.";
    case 'Q': return "--.-";
    case 'R': return ".-.";
    case 'S': return "...";
    case 'T': return "-";
    case 'U': return "..-";
    case 'V': return "...-";
    case 'W': return ".--";
    case 'X': return "-..-";
    case 'Y': return "-.--";
    case 'Z': return "--..";
    case ' ': return "     "; // Espaço, 7 unidades de silêncio
  }
}

void silencio() 
{
  delay(DOT);
}

void dot() 
{
  digitalWrite(MORSE_OUT_LED, HIGH);
  delay(DOT);
  digitalWrite(MORSE_OUT_LED, LOW);
  silencio();
}

void dash() 
{
  digitalWrite(MORSE_OUT_LED, HIGH);
  delay(DASH);
  digitalWrite(MORSE_OUT_LED, LOW);
  silencio();
}