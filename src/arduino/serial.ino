int cont = 0;

const String nomes[] = { "Ana", "Bruna", "Carlos", "Daniel" };

void setup() 
{
  Serial.begin(9600);
  
  Serial.print("Hello, world.");
}

void loop() 
{
  Serial.print("Hello, ");
  Serial.print(nomes[cont]);
  Serial.println("!");

  cont++;
  if (cont == 4) {
    cont = 0;
  }
  
  delay(500);
}