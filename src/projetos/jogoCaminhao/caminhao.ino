/*
 * Autor : Henrique Almeida Marcomini
 * Data 
 * 
 *  Toda e linha deste codigo foi escrita pelo autor do arquivo, todavia esta ideia foi encontradano site http://facacomarduino.info/projeto-38-jogo-dos-caminhoes-com-lcd.html?ckattempt=1
 *  , portanto este codigo é uma versao.
 * 
 *  Mençoes a Gabriela Mattos e a Isaac Mitsuaki que me ajudaram na construção e refinamento do codigo
 * 
 *  Copyright (c) 2015 Henrique Almeida Marcomini

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

// biblioteca

#include "rgb_lcd.h" //nescessario para usar o lcd da seed
#include <Wire.h> //indicado para usar em conjunto com "rgb_lcd.h"
#include <time.h> //usado como seed randomica

// declaração de variaveis

rgb_lcd lcd; // objeto lcd

int mapa[16]; //Mapa de posiçoes de cada "carro", 0 para inexistente, 1 para acima na linha 1, 2 para abaixo na linha 2, 3 para acima na linha 1, 3 para abaixo na linha 2
int posCarro = 3;//inicia o seu carro na posição 3
int temp = 0; //variavel auxiliar usada para gerar um carro a cada duas repetiçoes
int ponto = 0; //marcador da pontuação do jogador

byte carroCima[8] =// descrição do desenho do carro acima na linha
{
  B01110,
  B01110,
  B11111,
  B01010,
  B00000,
  B00000,
  B00000,
  B00000
};

byte carroBaixo[8] =// descrição do desenho do carro abaixo na linha
{
  B00000,
  B00000,
  B00000,
  B00000,
  B01110,
  B01110,
  B11111,
  B01010
};

bool crash = false; //variavel utilizada pra a verificação da "batida"

int i; // varaivel utilizada no aumento de velocidade
int tempo;// tempo de delay entre os loops

void setup()
{
  tempo = 400;// tempo de delay inicial
  ponto = 0; // pontuação inicial
  for(int i = 0; i<15 ; i++)// zera o vetor(por segurança)
  {
    mapa[i] = 0;
  }
  lcd.begin(2, 16); // inicia o lcd com 2 linhas e 16 colunas
  lcd.createChar(2, carroCima);//define o indice 2 como carro acima
  lcd.createChar(3, carroBaixo);//define o indice 3 como carro abaixo
  srand (time(NULL));// randomiza a seed
  pinMode(3, OUTPUT);//instacia o buzzer
  pinMode(4, INPUT);// instacia o botao
  //escreve a mensagem de boas vindas na tela
  lcd.setCursor(0,0);
  lcd.write("Aperte o botao");
  lcd.setCursor(0,1);
  lcd.write("para comecar");
  // aguarda o usuario apertar o botao
   while (digitalRead(4) == 0)
    {
      ;
    }
}
// loop principal do jogo
void loop()
{
  // este trecho serve para o aumento da velocidade
  i++;
  if(i==20 && tempo>100)
  {
    srand (time(NULL));
    tempo-=100;// diminui o delay
    i=0;//reseta o loop
    lcd.setRGB(rand() % 256,rand() % 256,rand() % 256);// deixa uma cor randomica na tela parqa notificar que a cor aumentou
  }
  // reposiciona o seu carro
  posCarro = (analogRead(A1) / 256) + 1;
  //caso nao bata
  if (!crash)
  {
    rodaArray();//joga cada um dos carros  uma posição para tras na array
    desenha();//desenha tudo na tela

  }
  // caso bata
  else
  {
    //exibe mensagem de derrota
    lcd.setRGB(255, 20, 20);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.write("Vish,");
    lcd.setCursor(0, 1);
    lcd.write(" voce perdeu");
    analogWrite(3, 5);
    delay(500);
    analogWrite(3, 0);
    crash = false;
    delay(2000);
    // exibe pontuação
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write("Voce fez ");
    lcd.setCursor(0,1);
    char pontos_c[10];
    sprintf(pontos_c, "%d", ponto);
    lcd.write(pontos_c);
    lcd.write(" pontos"); 
    delay(3500);
    // retorna para a mensagem de boas vindas
    setup();
  }
  delay(tempo);
}

void rodaArray()
{
  if (mapa[0] == posCarro)// verifica colisao
    crash = true;

    
  for (int i = 1; i < 15; i++)//"joga" o carro uma posição atraz na array
  {
    mapa[i - 1] = mapa[i];
    mapa[i] = 0;
  }
  if (temp == 0)// acrecenta uma novo carro
  {
    mapa[14] = (rand() % 4) + 1;
    temp = 1;
    ponto++;
  }
  else
    temp = 0; // reseta o temp
}


void desenha() // desenha cada um dos carro em suas devidas pósiçoes
{
  lcd.clear();
  desenhaCarro(posCarro, 0);
  for (int i = 0; i < 15; i++)
    desenhaCarro(mapa[i], i + 1);
}
void desenhaCarro(int i, int pos)
{
  switch (i)
  {
    case 1:
      lcd.setCursor(pos, 0);
      lcd.write(2);
      break;
    case 2:
      lcd.setCursor(pos, 0);
      lcd.write(3);
      break;
    case 3:
      lcd.setCursor(pos, 1);
      lcd.write(2);
      break;
    case 4:
      lcd.setCursor(pos, 1);
      lcd.write(3);
      break;
    default:
      break;
  }
}



