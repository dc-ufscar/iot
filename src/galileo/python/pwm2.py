#!/usr/bin/python

import mraa
import time
import os

PWM_PIN = 5           # Pino de saida digital  (PWM)
ADC_PIN = 0           # Pino de entrada analogico (ADC)
VAL_MAX = 1024.0      # Valor maximo lido no ADC

# Configuracao do PWM
pwm = mraa.Pwm(PWM_PIN)
pwm.period_us(5000)
pwm.enable(True)

# Configuracao do ADC
adc = mraa.Aio(ADC_PIN)

old_value = value = 1

while value:
    value = adc.read()             
    if (value != old_value):
        led_intensity = value/VAL_MAX  # Valor entre 0.0f e 1.0f 
        pwm.write(led_intensity)       
        print 'Process (', os.getpid(), ') value = ', value
    old_value = value
    time.sleep(1)

print 'Terminated (', os.getpid(), ')!'
 
